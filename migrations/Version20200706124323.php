<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200706124323 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE driver (id INT AUTO_INCREMENT NOT NULL, office_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_11667CD9FFA0C224 (office_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE driver_truck (driver_id INT NOT NULL, truck_id INT NOT NULL, INDEX IDX_4D4E3E27C3423909 (driver_id), INDEX IDX_4D4E3E27C6957CCE (truck_id), PRIMARY KEY(driver_id, truck_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE office (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE truck (id INT AUTO_INCREMENT NOT NULL, office_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_CDCCF30AFFA0C224 (office_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE driver ADD CONSTRAINT FK_11667CD9FFA0C224 FOREIGN KEY (office_id) REFERENCES office (id)');
        $this->addSql('ALTER TABLE driver_truck ADD CONSTRAINT FK_4D4E3E27C3423909 FOREIGN KEY (driver_id) REFERENCES driver (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE driver_truck ADD CONSTRAINT FK_4D4E3E27C6957CCE FOREIGN KEY (truck_id) REFERENCES truck (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30AFFA0C224 FOREIGN KEY (office_id) REFERENCES office (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE driver_truck DROP FOREIGN KEY FK_4D4E3E27C3423909');
        $this->addSql('ALTER TABLE driver DROP FOREIGN KEY FK_11667CD9FFA0C224');
        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30AFFA0C224');
        $this->addSql('ALTER TABLE driver_truck DROP FOREIGN KEY FK_4D4E3E27C6957CCE');
        $this->addSql('DROP TABLE driver');
        $this->addSql('DROP TABLE driver_truck');
        $this->addSql('DROP TABLE office');
        $this->addSql('DROP TABLE truck');
    }
}
