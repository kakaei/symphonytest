<?php

namespace App\Controller;

use App\Entity\Driver;
use App\Entity\Office;
use App\Entity\Truck;
use App\Form\Truck\TruckEditForm;
use App\Form\Truck\TruckForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class TruckController extends AbstractController
{
    /**
     * @Route("/truck", name="truck")
     */
    public function index()
    {
        $truck= $this->getDoctrine()->getRepository(Truck::class)->findAll();

        return $this->render('truck/index.html.twig', [
            'truckLists' => $truck,
        ]);
    }

    /**
     * @Route("/truck/new", name="truck_new")
     */
    public function new()
    {
        $form = $this->createForm(TruckForm::class,null,['action' => '/truck/create','method' => 'GET']);

        return $this->render('truck/new.html.twig',['truckForm'=> $form->createView()]);
    }

    /**
     * @Route("/truck/create",name="truck_create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function Create(Request $request)
    {
        $truckForm=$request->query->get('truck_form');
        $office= $this->getDoctrine()->getRepository(Office::class)->findOneBy(['id'=>$truckForm['office']]);
        $truck = new Truck();
        $truck->setName($truckForm['name']);
        $truck->setOffice($office);
        $docIn = $this->getDoctrine()->getManager();
        $docIn->persist($truck);

        $docIn->flush();

        return $this->redirect('/truck');
    }

    /**
     * @Route("/truck/edit/{id}",name="truck_edit")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function edit($id)
    {
        $data= $this->getDoctrine()->getRepository(Truck::class)->findOneBy(['id'=>$id]);

        $form = $this->createForm(TruckEditForm::class,$data,['action' => '/truck/update','method' => 'GET']);

        return $this->render('truck/edit.html.twig',['truckEditForm'=> $form->createView()]);
    }

    /**
     * @Route("/truck/update",name="truck_update")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function Update(Request $request)
    {
        $truckEditForm=$request->query->get('truck_edit_form');
        $entityManager = $this->getDoctrine()->getManager();



        $truck= $this->getDoctrine()->getRepository(Truck::class)->findOneBy(['id'=>$truckEditForm['id']]);

        foreach ($truck->getDriver() as $item) {
            $truck->removeDriver($item);
        }

        if (!$truck) {
            throw $this->createNotFoundException(
                'No product found for id '.$truckEditForm->id
            );
        }

        $office= $this->getDoctrine()->getRepository(Office::class)->findOneBy(['id'=>$truckEditForm['office']]);

        $truck->setName($truckEditForm['name']);
        $truck->setOffice($office);

        if(count($truckEditForm['driver']) >0 ){


            foreach ($truckEditForm['driver'] as $item) {
                $driver= $this->getDoctrine()->getRepository(Driver::class)->findOneBy(['id'=>$item]);
                $truck->addDriver($driver);
            }
        }



        $entityManager->flush();

        return $this->redirect('/truck');
    }

    /**
     * @Route("/truck/remove/{id}",name="office_remove")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function Remove($id)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $truck= $this->getDoctrine()->getRepository(Truck::class)->findOneBy(['id'=>$id]);

        $entityManager->remove($truck);

        $entityManager->flush();

        return $this->redirect('/truck');


    }
}
