<?php

namespace App\Controller;

use App\Entity\Driver;
use App\Entity\Office;
use App\Entity\Truck;
use App\Form\Driver\DriverEditForm;
use App\Form\Driver\DriverForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DriverController extends AbstractController
{
    /**
     * @Route("/driver", name="driver")
     */
    public function index()
    {
        $driver= $this->getDoctrine()->getRepository(Driver::class)->findAll();

        return $this->render('driver/index.html.twig', [
            'driverLists' => $driver,
        ]);
    }

    /**
     * @Route("/driver/new", name="driver_new")
     */
    public function new()
    {
        $form = $this->createForm(DriverForm::class,null,['action' => '/driver/create','method' => 'GET']);

        return $this->render('driver/new.html.twig',['driverForm'=> $form->createView()]);
    }

    /**
     * @Route("/driver/create",name="drive_create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function Create(Request $request)
    {

        $driverForm=$request->query->get('driver_form');

        $office= $this->getDoctrine()->getRepository(Office::class)->findOneBy(['id'=>$driverForm['office']]);
        $driver = new Driver();
        $driver->setName($driverForm['name']);
        $driver->setOffice($office);
        if(count($driverForm['truck']) >0 ){
            foreach ($driverForm['truck'] as $item) {
                $truck= $this->getDoctrine()->getRepository(Truck::class)->findOneBy(['id'=>$item]);
                $driver->addTruck($truck);
            }
        }

        $docIn = $this->getDoctrine()->getManager();
        $docIn->persist($driver);

        $docIn->flush();

        return $this->redirect('/driver');
    }

    /**
     * @Route("/driver/edit/{id}",name="driver_edit")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function edit($id)
    {
        $data= $this->getDoctrine()->getRepository(Driver::class)->findOneBy(['id'=>$id]);

        $form = $this->createForm(DriverEditForm::class,$data,['action' => '/driver/update','method' => 'GET']);

        return $this->render('driver/edit.html.twig',['DriverEditForm'=> $form->createView()]);
    }

    /**
     * @Route("/driver/update",name="driver_update")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function Update(Request $request)
    {
        $driverEditForm=$request->query->get('driver_edit_form');
        $entityManager = $this->getDoctrine()->getManager();



        $driver= $this->getDoctrine()->getRepository(Driver::class)->findOneBy(['id'=>$driverEditForm['id']]);


        foreach ($driver->getTruck() as $item) {
            $driver->removeTruck($item);
        }


        if (!$driver) {
            throw $this->createNotFoundException(
                'No product found for id '.$driverEditForm->id
            );
        }

        $office= $this->getDoctrine()->getRepository(Office::class)->findOneBy(['id'=>$driverEditForm['office']]);

        $driver->setName($driverEditForm['name']);
        $driver->setOffice($office);

        if(count($driverEditForm['truck']) >0 ){


            foreach ($driverEditForm['truck'] as $item) {
                $truck= $this->getDoctrine()->getRepository(Truck::class)->findOneBy(['id'=>$item]);
                $driver->addTruck($truck);
            }
        }


        $entityManager->flush();

        return $this->redirect('/driver');
    }

    /**
     * @Route("/driver/remove/{id}",name="driver_remove")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function Remove($id)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $driver= $this->getDoctrine()->getRepository(Driver::class)->findOneBy(['id'=>$id]);

        $entityManager->remove($driver);

        $entityManager->flush();

        return $this->redirect('/driver');


    }


}
