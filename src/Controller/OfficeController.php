<?php

namespace App\Controller;

use App\Entity\Office;
use App\Form\OfficeEditForm;
use App\Form\OfficeForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OfficeController extends AbstractController
{

    /**
     * @Route("/office", name="office")
     */
    public function index()
    {
        $office= $this->getDoctrine()->getRepository(Office::class)->findAll();


        return $this->render('office/index.html.twig', [
            'officeLists' => $office,
        ]);
    }

    /**
     * @Route("/office/new", name="office_new")
     */
    public function new()
    {
        $form = $this->createForm(OfficeForm::class,null,['action' => '/office/create','method' => 'GET']);

        return $this->render('office/new.html.twig',['officeForm'=> $form->createView()]);
    }

    /**
     * @Route("/office/create",name="Office_create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function Create(Request $request)
    {
        $officeForm=$request->query->get('office_form');

        $office = new Office();
        $office->setName($officeForm['name']);
        $docIn = $this->getDoctrine()->getManager();
        $docIn->persist($office);

        $docIn->flush();

        return $this->redirect('/office');
    }

    /**
     * @Route("/office/edit/{id}",name="office_edit")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function edit($id)
    {
        $data= $this->getDoctrine()->getRepository(Office::class)->findOneBy(['id'=>$id]);

        $form = $this->createForm(OfficeEditForm::class,$data,['action' => '/office/update','method' => 'GET']);

        return $this->render('office/edit.html.twig',['officeEditForm'=> $form->createView()]);
    }

    /**
     * @Route("/office/update",name="office_update")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function Update(Request $request)
    {
        $officeEditForm=$request->query->get('office_edit_form');
        $entityManager = $this->getDoctrine()->getManager();

        $office= $this->getDoctrine()->getRepository(Office::class)->findOneBy(['id'=>$officeEditForm['id']]);

        if (!$office) {
            throw $this->createNotFoundException(
                'No product found for id '.$officeEditForm->id
            );
        }

        $office->setName($officeEditForm['name']);
        $entityManager->flush();

        return $this->redirect('/office');
    }

    /**
     * @Route("/office/remove/{id}",name="office_remove")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function Remove($id)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $office= $this->getDoctrine()->getRepository(Office::class)->findOneBy(['id'=>$id]);

        $entityManager->remove($office);

        $entityManager->flush();

        return $this->redirect('/office');


    }


}
