<?php

namespace App\Entity;

use App\Repository\DriverRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DriverRepository::class)
 */
class Driver
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Office::class, inversedBy="drivers")
     */
    private $Office;

    /**
     * @ORM\ManyToMany(targetEntity=Truck::class, inversedBy="drivers")
     */
    private $Truck;

    public function __construct()
    {
        $this->Truck = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOffice(): ?Office
    {
        return $this->Office;
    }

    public function setOffice(?Office $Office): self
    {
        $this->Office = $Office;

        return $this;
    }

    /**
     * @return Collection|Truck[]
     */
    public function getTruck(): Collection
    {
        return $this->Truck;
    }

    public function addTruck(Truck $truck): self
    {
        if (!$this->Truck->contains($truck)) {
            $this->Truck[] = $truck;
        }

        return $this;
    }

    public function removeTruck(Truck $truck): self
    {
        if ($this->Truck->contains($truck)) {
            $this->Truck->removeElement($truck);
        }

        return $this;
    }
}
