<?php


namespace App\Form\Driver;


use App\Entity\Office;
use App\Entity\Truck;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class DriverEditForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('office',EntityType::class,[
                'class'=>Office::class,
                'choice_label' => 'name',
            ])
            ->add('truck',EntityType::class,[
                'class'=>Truck::class,
                'choice_label' => 'name',
                'multiple' => true,
                'choice_value'=>'id'
            ])
            ->add('id',HiddenType::class)
            ->add('Update', SubmitType::class)
        ;
    }

}