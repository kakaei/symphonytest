<?php


namespace App\Form\Truck;


use App\Entity\Driver;
use App\Entity\Office;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class TruckForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('office',EntityType::class,[
                'class'=>Office::class,
                'choice_label' => 'name',
                'choice_value'=>'id'
            ])

            ->add('driver',EntityType::class,[
                'class'=>Driver::class,
                'choice_label' => 'name',
                'multiple' => true,
                'choice_value'=>'id'
            ])
            ->add('Create', SubmitType::class)
        ;
    }

}